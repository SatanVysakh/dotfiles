-- Basic Settings
vim.opt.number = true              -- Show line numbers
vim.opt.tabstop = 4                -- Number of spaces per tab
vim.opt.shiftwidth = 4             -- Number of spaces for auto-indentation
vim.opt.expandtab = true           -- Use spaces instead of tabs
vim.opt.smartindent = true         -- Enable smart indentation
vim.opt.wrap = false               -- Disable line wrapping
vim.opt.hidden = true              -- Enable buffer switching without saving
vim.opt.showcmd = true             -- Show (partial) command in status line

-- Highlight matching parentheses/brackets
vim.opt.showmatch = true

-- Enable syntax highlighting
vim.cmd('syntax enable')

vim.opt.clipboard = 'unnamedplus'

